<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S1: Activity</title>
</head>
<body>
    <div style='border: solid 1px; width: 25%; padding: 10px; margin: 5px 0px;'>
        <h1>Full Address</h1>
        <p><?php echo getFullAddress("Philippines", "Legazpi City", "Albay", "Brgy 2 Ems Barrio")?></p>
        <p><?php echo getFullAddress("Philippines", "Makati City", "Metro Manila", "3F Enzo Bldg., Buendia Avenue")?></p>
    </div>
    <div style='border: solid 1px; width: 25%; padding: 10px'>
        <h1>Letter-Based Grading</h1>
        <p><?php echo "87 is equivalent to ".getLetterGrade(87)?></p>
        <p><?php echo "94 is equivalent to ".getLetterGrade(94)?></p>
        <p><?php echo "74 is equivalent to ".getLetterGrade(74)?></p>
    </div>
</body>
</html>